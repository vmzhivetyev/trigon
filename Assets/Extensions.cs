﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public static class Extensions
{
    /// <summary>
    /// Is null OR without PlayerController OR PlayerController.isDead
    /// </summary>
    /// <param name="go"></param>
    /// <returns></returns>
    public static bool needRespawn(this GameObject go)
    {
        return !go || !go.GetComponent<PlayerController>() || go.GetComponent<PlayerController>().isDead;
    }

    /// <summary>
    /// Not null AND (No PlayerController OR PlayerController.isAlive)
    /// </summary>
    /// <param name="go"></param>
    /// <returns></returns>
    public static bool needDestroy(this GameObject go)
    {
        if (!go) return false;
        return !go.GetComponent<PlayerController>() || !go.GetComponent<PlayerController>().isDead;
    }
}

public class MyNetworkBehaviour : NetworkBehaviour
{
    public bool isMine { get; private set; }

    public virtual void Start()
    {
        print("my net beh, ismine: " + isLocalPlayer);
        isMine = isLocalPlayer;
    }
}
