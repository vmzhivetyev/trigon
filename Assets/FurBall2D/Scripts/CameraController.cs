﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public Transform Player;
    [SerializeField]
    private float m_speed = 0.1f;
    Camera cam;

    public void Start()
    {
        cam = GetComponent<Camera>();
    }

    public void Update()
    {
        if (Player)
            transform.position = Vector3.Lerp(transform.position, Player.position, m_speed) + new Vector3(0, 0, -12);
    }
}
