﻿using UnityEngine;

public class Cam2D : MonoBehaviour
{
    public Vector3 targetPos
    {
        get { return (Vector3)_target.worldCenterOfMass + Vector3.forward * zOffset; }
    }

    public Transform Target
    {
        set
        {
            _target = value.GetComponent<Rigidbody2D>();
            transform.position = targetPos;
        }
    }
    [SerializeField]
    private Rigidbody2D _target;

    [SerializeField]
    private float damping = 1;
    [SerializeField]
    private float zOffset = -10;
    [SerializeField]
    private float snapDistance = 3;

    private Vector3 m_CurrentVelocity;

    private void LateUpdate()
    {
        if (!_target)
            return;

        if (Vector3.Distance(transform.position, targetPos) > snapDistance)
            transform.position = targetPos;

        transform.position = Vector3.Lerp (transform.position, targetPos, damping * Time.deltaTime);
    }
}