﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerIdentity : MyNetworkBehaviour
{
    public static PlayerIdentity singleton;

    internal GameObject currPlayerInstance;
    internal GameObject canvas;

    public override void Start()
    {
        base.Start();

        canvas = transform.GetComponentInChildren<Canvas>().gameObject;
        canvas.SetActive(false);

        if (isMine)
        {
            singleton = this;
            canvas.SetActive(true);
        }
        else
        {
            Destroy(canvas);
        }
    }

    [Command]
    private void CmdRespawn()
    {
        if (!currPlayerInstance.needRespawn())
            return;

        print("Respawning. ConnectionId: " + connectionToClient.connectionId);

        currPlayerInstance = (GameObject)Instantiate(NetworkManager.singleton.playerPrefab, new Vector3(Random.Range(-10, 10), Random.Range(-10, 10), 0), Quaternion.identity);
        NetworkServer.ReplacePlayerForConnection(connectionToClient, currPlayerInstance, playerControllerId);
    }

    public void OnBtnRespawnClicked()
    {
        if(isMine)
        {
            CmdRespawn();
        }
    }

    private void Update()
    {
        if (isMine)
            canvas.SetActive(currPlayerInstance.needRespawn());
    }
}
