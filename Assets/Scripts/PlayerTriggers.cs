﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerTriggers : MonoBehaviour
{
    [SerializeField]
    private bool forceTriggers = false;

    private void Start()
    {
        if (!transform.parent.GetComponent<PlayerController>().isServer)
            for (int i = 0; i < transform.parent.childCount; i++)
                Destroy(transform.parent.GetChild(i).gameObject);

        if(forceTriggers)
            foreach (var c in GetComponents<Collider2D>())
                c.isTrigger = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        OnTriggerEnter2D(collision.collider);
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        var root_c = transform.root.GetComponent<PlayerController>();
        var victim_c = coll.transform.root.GetComponent<PlayerController>();

        if (coll.gameObject.layer == LayerMask.NameToLayer("Vertices") || coll.isTrigger)
            return;

        if (!root_c || !victim_c || root_c.isDead || transform.root == coll.transform.root)
            return;
        
        var me_t = transform.parent;
        var other_t = coll.transform.parent;

        var me_c = me_t.GetComponent<PlayerController>();
        var other_c = other_t.GetComponent<PlayerController>();

        if (other_c.wasLastAttached + Settings.attachDelay > Time.time || me_c.wasLastAttached + Settings.attachDelay > Time.time)
            return;

        other_c.wasLastAttached = Time.time;
        me_c.gotLastAttach = Time.time;

        print(root_c.name + " attached " + other_t.name);

        //destroy childs
        for (int i = 0; i < other_t.childCount; i++)
        {
            var ch = other_t.GetChild(i);
            if (ch.GetComponent<Collider2D>())
                continue;
            NetworkServer.Destroy(ch.gameObject);
            ch.SetParent(null);
        }

        //reattach
        other_t.SetParent(me_t);
        other_t.GetComponent<NetworkTransform>().sendInterval = 0;
        other_t.GetComponent<NetworkTransform>().enabled = false;
        Destroy(other_t.GetComponent<Rigidbody2D>());

        //reattach on network
        other_c.CallNetworkReattach(me_c.gameObject);

        //var ntc = me_c.gameObject.AddComponent<NetworkTransformChild>();
        //ntc.target = other_t;
    }
}
