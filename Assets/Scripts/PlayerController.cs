﻿using UnityEngine;
using CnControls;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour
{
    public bool debugColor = false;

    public bool isDead
    {
        get { return _dead; }
        private set { _dead = value; }
    }

    [SerializeField][SyncVar] private float speed = 5;
    [SerializeField][SyncVar] private float turnSpeed = 2;
    [SerializeField][SyncVar] private bool _dead;
    [SerializeField][SyncVar] private Color c;

    [SerializeField] private Color myColor;
    [SerializeField] internal float wasLastAttached;
    [SerializeField] internal float gotLastAttach;
    [SerializeField] private Vector2 inp;

    [ServerCallback]
    private void Awake()
    {
        wasLastAttached = Time.time;

        myColor = Settings.RandomColor;
    }

    private void Start()
    {
        GenerateBody(gameObject, myColor);

        if(isLocalPlayer)
        {
            Camera.main.GetComponent<Cam2D>().Target = transform;
            PlayerIdentity.singleton.currPlayerInstance = gameObject;
        }
    }

    private void Update()
    {
        Move();
        Colorize();
    }

    [Command]
    private void CmdSetInp(Vector2 i)
    {
        //print("cmdsetinp " + i);
        inp = i;
    }
    
    private void Move()
    {
        if (isLocalPlayer)
        {
            inp = new Vector2(CnInputManager.GetAxis("Horizontal"), CnInputManager.GetAxis("Vertical"));
            CmdSetInp(inp);
        }

        if (isServer && !isDead)
        {
            //print("AddForce(" + (inp * speed) + ")");
            GetComponent<Rigidbody2D>().AddForce(inp * speed);

            if (inp == Vector2.zero)
                return;
            float angle = Mathf.Clamp(Vector2.Angle(inp, transform.up), 0, 90);
            float sign = Mathf.Sign(Vector3.Cross(transform.up, inp).z);
            GetComponent<Rigidbody2D>().AddTorque(Mathf.Sin(Mathf.Deg2Rad * angle) * sign * turnSpeed);
        }
    }
    
    private void Colorize()
    {
        if (isServer)
        {
            if (debugColor)
            {
                if (wasLastAttached + Settings.attachDelay > Time.time)
                    c = Color.red;
                else if (gotLastAttach + Settings.attachDelay > Time.time)
                    c = Color.green;
                else
                    c = myColor;
            }
            else
            {
                c = myColor;
            }
        }

        GetComponent<MeshRenderer>().sharedMaterial.color = c;
    }

    //called by CallNetworkReattach on same object
    [ClientRpc]
    private void RpcTellDeath(GameObject killer_g, Vector3 locPos, Quaternion locRot)
    {
        isDead = true;

        Transform killer_t = killer_g.transform;
        transform.SetParent(killer_t);

        //don't need to set it on clients
        GetComponent<NetworkTransform>().enabled = false;
        Destroy(GetComponent<Rigidbody2D>());
        transform.localPosition = locPos;
        transform.localRotation = locRot;
    }

    internal void CallNetworkReattach(GameObject killer_g)
    {
        RpcTellDeath(killer_g, transform.localPosition, transform.localRotation);
    }

    private GameObject GenerateBody(GameObject go, Color color, Texture2D texture = null)
    {
        var meshFilter = go.GetComponent<MeshFilter>();
        if (!meshFilter)
            meshFilter = go.AddComponent<MeshFilter>();

        var renderer = go.GetComponent<MeshRenderer>();
        if (!renderer)
            renderer = go.AddComponent<MeshRenderer>();
        
        meshFilter.sharedMesh = CreateMesh(1);

        var tex = texture == null ? new Texture2D(1, 1) : texture;
        if (!texture)
        {
            tex.SetPixel(0, 0, Color.white);
            tex.Apply();
        }

        renderer.sharedMaterial = new Material(Shader.Find("Sprites/Default"));
        renderer.sharedMaterial.mainTexture = tex;
        renderer.sharedMaterial.color = color;

        return go;
    }

    private Mesh CreateMesh(float size)
    {
        Mesh m = new Mesh();
        m.name = "ScriptedMesh";
        //float a = size / 2 / Mathf.Tan(30 * Mathf.Deg2Rad);
        float b = size / 2 * Mathf.Tan(60 * Mathf.Deg2Rad);//-a
        m.vertices = new Vector3[] {
             new Vector3(-size/2, 0, 0),
             new Vector3(size/2, 0, 0),
             new Vector3(0, b, 0)
         };
        m.uv = new Vector2[] {
             new Vector2 (0, 0),
             new Vector2 (0, 1),
             new Vector2(1, 1)
         };
        m.triangles = new int[] { 0, 1, 2 };
        m.RecalculateNormals();

        return m;
    }
}
