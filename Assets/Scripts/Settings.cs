﻿using UnityEngine;
using System.Collections;

public static class Settings
{
    public static float attachDelay = 2;
    
    public static Color RandomColor { get { return Random.ColorHSV(0, 1, 1, 1, 1, 1); } }
}
