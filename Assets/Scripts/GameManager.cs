﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Linq;

public class GameManager : NetworkManager
{
    private void Start()
    {
        Application.targetFrameRate = 60;
    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        GameObject ident = Instantiate(spawnPrefabs[0]);
        NetworkServer.AddPlayerForConnection(conn, ident, playerControllerId);
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        //destroy player's triangle if it isn't dead
        var player = conn.playerControllers[0].gameObject;
        if (player.needDestroy())
            NetworkServer.DestroyPlayersForConnection(conn);

        //destroy his identity and all other objects
        foreach (var netid in conn.clientOwnedObjects)
        {
            var obj = NetworkServer.FindLocalObject(netid);

            //don't delete dead player bodies
            if (obj.needDestroy())
                NetworkServer.Destroy(obj);
        }
    }
}
